import Vue from 'vue/dist/vue.common'
import { Terminal } from 'xterm'
import * as fit from 'xterm/lib/addons/fit/fit'
import * as aframe from 'aframe'
import * as triangleset from 'aframe-triangleset-component'
import * as simple_sun_sky from 'aframe-simple-sun-sky'
import * as ocean from 'aframe-extras.ocean'
import * as orbit from 'aframe-particle-system-component'
import * as THREE from 'three'
import Martini from '@mapbox/martini'

const PIXELATE = 4

Vue.config.ignoredElements = [
      'a-scene',
      'a-entity',
      'a-camera',
      'a-box',
      'a-plane',
      'a-sphere',
      'a-cylinder',
      'a-sky',
      'a-triangleset'
    ]

Terminal.applyAddon(fit)

startUp(Vue, Terminal, Martini)

new Vue({el:"#app"})

function onLock() {
        if (document.pointerLockElement!==null) document.pointerLockElement.focus() 
    }
document.addEventListener('pointerlockchange', onLock)
document.addEventListener('mozpointerlockchange', onLock)
            
aframe.registerComponent('post-proc', {
  init: function () { 
    this.el.renderer.setPixelRatio(window.devicePixelRatio / PIXELATE)
    this.el.renderer.shadowMap.enabled = true
    this.el.renderer.shadowMap.type = THREE.PCFShadowMap
    this.el.renderer.gammaOutput = true
  }
});
