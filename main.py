import base64
import os
import sys
import wuy
import vbuild
import json


class index(wuy.Server):

    def beep(self):
        print("\a BEEP !!!")

    def get_height_map(self):
        return json.loads(open('data/frag.json').read())

    def get_height_img(self):
        return base64.b64encode(open("data/frag.jpg", "rb").read())

    def _render(self, path):
        template = open("web/template.html").read()
        v = vbuild.render(os.path.join(sys.path[0], "web/*.vue"))
        template = template.replace("<!-- SCRIPT -->", str(v.script))
        template = template.replace("<!-- STYLE -->", str(v.style))
        return template.replace("<!-- HTML -->", str(v.html))


index()
